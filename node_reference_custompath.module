<?php

/**
 * @file
 * Formatter to allow a node reference to point to a custom path.
 */

/**
 * Implements hook_field_formatter_info().
 */
function node_reference_custompath_field_formatter_info() {
  $ret = array(
    'node_reference_custompath' => array(
      'label' => t('Internal Path'),
      'field types' => array('node_reference'),
      'settings' => array(
        'path' => '',
        'targetwindow' => 'current',
      ),
    ),
  );
  return $ret;
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function node_reference_custompath_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  if ($display['type'] == 'node_reference_custompath') {
    $element['path'] = array(
      '#type' => 'textfield',
      '#title' => t('Linked Custom Path'),
      '#description' => t('Display the title of the referenced node linked to a custom internal path'),
      '#default_value' => $settings['path'],
    );
    
    $element['targetwindow']  = array(
      '#type' => 'radios',
      '#title' => t('Open in'),
      '#default_value' => $settings['targetwindow'],
      '#options' => array(
        '_blank' => t('New Window'),
        'current' => t('Current Window'),
      ),
    );

    if (module_exists('token') == TRUE) {
      $element['tokens'] = array(
        '#title' => t('Tokens'),
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );
      $element['tokens']['help'] = array(
        '#theme' => 'token_tree',
        '#token_types' => 'all',
        '#global_types' => FALSE,
      );
    }
  }

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function node_reference_custompath_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $summary = array();

  if ($display['type'] == 'node_reference_custompath') {
    if (empty($settings['path']) === FALSE) {
      $summary[] = t('Path: @link', array('@link' => $settings['path']));
      $summary[] = t('Window: @window', array('@window' => $settings['targetwindow']));
    }
    else {
      $summary[] = t('Not linked');
    }
  }

  return implode('<br />', $summary);
}

/**
 * Implements hook_field_formatter_prepare_view().
 */
function node_reference_custompath_field_formatter_prepare_view($entity_type, $entities, $field, $instances, $langcode, &$items, $displays) {
  // Load the referenced nodes, except for the 'node_reference_nid' which does
  // not need full objects.

  // Collect ids to load.
  $ids = array();
  foreach ($displays as $id => $display) {
    foreach ($items[$id] as $delta => $item) {
      if ($item['access']) {
        $ids[$item['nid']] = $item['nid'];
      }
    }
  }
  $entities = node_load_multiple($ids);

  // Add the loaded nodes to the items.
  foreach ($displays as $id => $display) {
    foreach ($items[$id] as $delta => $item) {
      if ($item['access']) {
        $items[$id][$delta]['node'] = $entities[$item['nid']];
      }
    }
  }
}

/**
 * Implements hook_field_formatter_view().
 */
function node_reference_custompath_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  global $user;

  $result = array();
  $settings = $display['settings'];

  switch ($display['type']) {
    case 'node_reference_custompath':
      foreach ($items as $delta => $item) {
        $node = $item['node'];
        if (empty($settings['path']) === FALSE) {
          $path = token_replace($settings['path'], array('node' => $node, 'user' => $user));
        }
        else {
          $path = '';
        }
        $params = drupal_parse_url($path);

        // If we don't have a path just output the name.
        if (empty($path) === FALSE) {
          $result[$delta] = array(
            '#type' => 'link',
            '#title' => $node->title,
            '#href' => $params['path'],
            '#options' => array(
              'query' => $params['query'],
              'fragment' => $params['fragment'],
            ),
          );
          if ($settings['targetwindow']) {
            $result[$delta]['#options']['attributes']['target'] = $settings['targetwindow'];
          }
        }
        else {
          $result[$delta] = array('#markup' => $node->title);
        }
      }
      break;
  }

  return $result;
}
